﻿using AccidentsWeb.Models;
using AccidentsWeb.Models.APIModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AccidentsWeb.Controllers.Api
{
    public class MetricsApiController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        /// <summary>
        /// API for metrics of max, min and average
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("api/Metrics")]
        public IEnumerable<AccidentMetricsViewModel> GetAverage()
        {
            var _max = (from c in db.Accidents select c).Max(c => c.VehicleCount).ToString();
            var _min = (from c in db.Accidents select c).Min(c => c.VehicleCount).ToString();
            var _avg = (from c in db.Accidents select c).Average(c => c.VehicleCount).ToString();

            AccidentMetricsViewModel[] av = new AccidentMetricsViewModel[]
            {
            new AccidentMetricsViewModel { Metric = "Max", Count = _max },
            new AccidentMetricsViewModel { Metric = "Min", Count = _min },
            new AccidentMetricsViewModel { Metric = "Average", Count = _avg }
            };

            return av;
        }

    }
}
