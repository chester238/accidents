﻿using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using AccidentsWeb.Models;

namespace AccidentsWeb.Controllers
{
    /// <summary>
    /// API for handling data from the database relating to Accidents.
    /// This can be expanded into its own project for re-use
    /// Built with scalability in mind
    /// </summary>
    public class AccidentsAPIController : ApiController
    {
        
        private ApplicationDbContext db = new ApplicationDbContext();

        // Returns all accidents from the DB
        [HttpGet]
        [Route("api/AccidentsAPI")]
        public IQueryable<Accident> GetAccidents()
        {
            return db.Accidents;
        }

        // Returns all aggregated by Month
        [HttpGet]
        [Route("api/AggregatedAccidents")]
        public IQueryable<AccidentsGroupViewModel> GetAggregated()
        {
            var groupedResults = db.Accidents
        .GroupBy(o => new
        {
            AccMonth = o.AccidentDate.Month,
            AccYear = o.AccidentDate.Year
        })
        .Select(g => new AccidentsGroupViewModel
        {
            Month = g.Key.AccMonth,
            Year = g.Key.AccYear,
            Total = g.Count(),
            Percentage = (g.Count() * 100 / db.Accidents.Count()) //Calculates accidents for the month as a percentage of the total
        })
        .OrderByDescending(a => a.Year)
        .ThenByDescending(a => a.Month);

            return groupedResults;
        }
              
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}