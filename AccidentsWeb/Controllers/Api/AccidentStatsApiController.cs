﻿using AccidentsWeb.Models;
using AccidentsWeb.Models.ViewModels;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Http;

namespace AccidentsWeb.Controllers.Api
{
    public class AccidentStatsApiController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        /// <summary>
        /// Rush hour and outside rush hour analysis. Results grouped by month and year
        /// </summary>
        /// <returns>List of accidents in rush hour and outside</returns>
        [HttpGet]
        [Route("api/RushHourStats")]
        public IEnumerable<RushHourViewModel> GetRushStas()
        {
            List<RushHourViewModel> rshList = new List<RushHourViewModel>();

            var statsResults = db.Accidents
        .GroupBy(o => new
        {
            AccMonth = o.AccidentDate.Month,
            AccYear = o.AccidentDate.Year
        }).Select(g => new RushHourDto
        {
            RushHourCount = g.Count(t => t.AccidentTime.Hour == 8 || t.AccidentTime.Hour == 9 ||
            t.AccidentTime.Hour == 16 || t.AccidentTime.Hour == 17), //Rush hours between 8-9:59 and 4-5:59
            TotalCount = g.Count(), //Total items in grouping
            Month = g.Key.AccMonth,
            Year = g.Key.AccYear
        }).ToList();

            foreach (var res in statsResults)
            {
                var month = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(res.Month);
                var year = res.Year;
                var nonRushHourCalc = res.TotalCount - res.RushHourCount; //Calculate value of non rush hour volume

                rshList.Add(new RushHourViewModel()
                {
                    TimeFrame = month + "-" + year,
                    PercentageRush = (res.RushHourCount * 100 / res.TotalCount),
                    PercentageNonRush = (nonRushHourCalc * 100 / res.TotalCount)
                });
            }
            return rshList;
        }
    }
}
