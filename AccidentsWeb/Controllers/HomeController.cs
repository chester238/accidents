﻿using AccidentsWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AccidentsWeb.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Metric()
        {
            ViewBag.Message = "Metric dashboard";

            return View();
        }

        public ActionResult RushHour()
        {
            ViewBag.Message = "Rush Hour";

            return View();
        }
    }
}