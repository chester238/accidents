﻿using System.Web;
using System.Web.Optimization;

namespace AccidentsWeb
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/scripts").Include(
                        "~/Scripts/jquery-3.3.1.min.js",
                        "~/Scripts/bootstrap.min.js",
                        "~/scripts/datatables/jquery.datatables.min.js",
                        "~/scripts/datatables/dataTables.buttons.min.js",
                        "~/scripts/datatables/buttons.html5.min.js",
                        "~/scripts/datatables/datatables.bootstrap.js",
                        "~/scripts/jszip.min.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            //bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
            //          "~/Scripts/bootstrap.js",
            //          "~/Scripts/respond.js")); ///Merged with scriptbundle above

            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/bootstrap-lumen.css",
                      "~/Content/bootstrap-theme.css",
                      "~/content/datatables/css/jquery.dataTables.min.css",
                      "~/content/datatables/css/buttons.dataTables.min.css",
                      "~/Content/site.css"));
        }
    }
}
