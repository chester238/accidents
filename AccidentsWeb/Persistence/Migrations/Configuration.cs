using System.Collections.Generic;
using System.Globalization;
using AccidentsWeb.Models;

namespace AccidentsWeb.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<AccidentsWeb.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            MigrationsDirectory = @"Persistence\Migrations";
        }

        protected override void Seed(AccidentsWeb.Models.ApplicationDbContext context)
        {
            CultureInfo provider = CultureInfo.InvariantCulture;
            var format = "dd/MM/yyyy HH:mm"; //Date format for seeding
            List<Accident> acc = new List<Accident>();

            context.Accidents.AddOrUpdate(
                new Accident
                {
                    Reference = "123456",
                    GridEasting = "418241",
                    GridNorthing = "442351",
                    VehicleCount = 2,
                    VehicleType = "Motorcycle over 500cc",
                    Express = null,
                    AccidentDate = DateTime.ParseExact("15/06/2016 08:30", format, provider),
                    AccidentTime = DateTime.ParseExact("15/06/2016 08:30", format, provider),
                    RoadClass = "A",
                    RoadSurface = "Dry",
                    LightingConditions = "Darkness: street lights present and lit",
                    WeatherConditions = "Fine without high winds",
                    CasualtyClass = "Driver or rider",
                    CasualtySeverity = "Serious",
                    CasualtySex = "Male",
                    DateOfBirth = DateTime.ParseExact("01/11/1990 00:00", format, provider)
                },
                new Accident
                {
                    Reference = "34567",
                    GridEasting = "428241",
                    GridNorthing = "342351",
                    VehicleCount = 1,
                    VehicleType = "Car",
                    Express = null,
                    AccidentDate = DateTime.ParseExact("11/07/2016 11:20", format, provider),
                    AccidentTime = DateTime.ParseExact("11/07/2016 11:20", format, provider),
                    RoadClass = "Motorway",
                    RoadSurface = "Wet / Damp",
                    LightingConditions = "Daylight: street lights present",
                    WeatherConditions = "Raining without high winds",
                    CasualtyClass = "Driver or rider",
                    CasualtySeverity = "Slight",
                    CasualtySex = "Female",
                    DateOfBirth = DateTime.ParseExact("01/10/1980 00:00", format, provider)
                }, new Accident
                {
                    Reference = "23456",
                    GridEasting = "433241",
                    GridNorthing = "300351",
                    VehicleCount = 10,
                    VehicleType = "Car",
                    Express = null,
                    AccidentDate = DateTime.ParseExact("11/07/2016 07:30", format, provider),
                    AccidentTime = DateTime.ParseExact("11/07/2016 07:30", format, provider),
                    RoadClass = "Motorway",
                    RoadSurface = "Wet / Damp",
                    LightingConditions = "Daylight: street lights present",
                    WeatherConditions = "Raining without high winds",
                    CasualtyClass = "Driver or rider",
                    CasualtySeverity = "Slight",
                    CasualtySex = "Female",
                    DateOfBirth = DateTime.ParseExact("01/11/1990 00:00", format, provider)
                }, new Accident
                {
                    Reference = "908666",
                    GridEasting = "318241",
                    GridNorthing = "444351",
                    VehicleCount = 1,
                    VehicleType = "Car",
                    Express = null,
                    AccidentDate = DateTime.ParseExact("11/08/2016 09:30", format, provider),
                    AccidentTime = DateTime.ParseExact("11/08/2016 09:30", format, provider),
                    RoadClass = "Unclassified",
                    RoadSurface = "Dry",
                    LightingConditions = "Darkness: street lights present and lit",
                    WeatherConditions = "Fine without high winds",
                    CasualtyClass = "Driver or rider",
                    CasualtySeverity = "Slight",
                    CasualtySex = "Male",
                    DateOfBirth = DateTime.ParseExact("01/11/2000 00:00", format, provider)
                }, new Accident
                {
                    Reference = "434343",
                    GridEasting = "418244",
                    GridNorthing = "442331",
                    VehicleCount = 2,
                    VehicleType = "Pedal Cycle",
                    Express = null,
                    AccidentDate = DateTime.ParseExact("15/08/2016 08:30", format, provider),
                    AccidentTime = DateTime.ParseExact("15/08/2016 08:30", format, provider),
                    RoadClass = "A",
                    RoadSurface = "Dry",
                    LightingConditions = "Darkness: street lights present and lit",
                    WeatherConditions = "Fine without high winds",
                    CasualtyClass = "Driver or rider",
                    CasualtySeverity = "Serious",
                    CasualtySex = "Male",
                    DateOfBirth = DateTime.ParseExact("01/11/1990 00:00", format, provider)
                },
                new Accident
                {
                    Reference = "123456",GridEasting = "418241",GridNorthing = "442351",VehicleCount = 2,
                    VehicleType = "Motorcycle over 500cc",Express = null,
                    AccidentDate = DateTime.ParseExact("15/08/2016 10:30", format, provider),
                    AccidentTime = DateTime.ParseExact("15/08/2016 10:30", format, provider),
                    RoadClass = "A",RoadSurface = "Dry",LightingConditions = "Darkness: street lights present and lit",
                    WeatherConditions = "Fine without high winds",CasualtyClass = "Driver or rider",CasualtySeverity = "Serious",
                    CasualtySex = "Female",DateOfBirth = DateTime.ParseExact("01/12/1975 00:00", format, provider)
                });
        }
    }
}
