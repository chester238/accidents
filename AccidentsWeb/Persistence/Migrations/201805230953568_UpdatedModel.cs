namespace AccidentsWeb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdatedModel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Accidents", "DateOfBirth", c => c.DateTime(nullable: false));
            DropColumn("dbo.Accidents", "CasualtyAge");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Accidents", "CasualtyAge", c => c.Int(nullable: false));
            DropColumn("dbo.Accidents", "DateOfBirth");
        }
    }
}
