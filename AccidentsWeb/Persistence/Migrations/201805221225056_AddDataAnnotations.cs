namespace AccidentsWeb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDataAnnotations : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Accidents", "Reference", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Accidents", "Reference", c => c.String());
        }
    }
}
