namespace AccidentsWeb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Accidents",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Reference = c.String(),
                        GridEasting = c.String(),
                        GridNorthing = c.String(),
                        VehicleCount = c.Int(nullable: false),
                        VehicleType = c.String(),
                        Express = c.String(),
                        AccidentDate = c.DateTime(nullable: false),
                        AccidentTime = c.DateTime(nullable: false),
                        RoadClass = c.String(),
                        RoadSurface = c.String(),
                        LightingConditions = c.String(),
                        WeatherConditions = c.String(),
                        CasualtyClass = c.String(),
                        CasualtySeverity = c.String(),
                        CasualtySex = c.String(),
                        CasualtyAge = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Accidents");
        }
    }
}
