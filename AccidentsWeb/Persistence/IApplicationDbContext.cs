﻿using AccidentsWeb.Models;
using System.Data.Entity;

namespace AccidentsWeb.Persistence
{
    public interface IApplicationDbContext
    {
        DbSet<Accident> Accidents { get; set; }
    }
}