﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AccidentsWeb.Models
{
    public class RushHourViewModel
    {
        public string TimeFrame { get; set; }
        public int PercentageRush { get; set; }
        public int PercentageNonRush { get; set; }
    }
}