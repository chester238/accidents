﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AccidentsWeb.Models.ViewModels
{
    public class RushHourDto
    {
        public int RushHourCount { get; set; }
        public int TotalCount { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
    }
}