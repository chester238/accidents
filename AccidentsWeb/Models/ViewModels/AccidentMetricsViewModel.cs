﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AccidentsWeb.Models.APIModels
{
    public class AccidentMetricsViewModel
    {
        public string Metric { get; set; }
        public string Count { get; set; }
    }
}