﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace AccidentsWeb.Models
{
    public class AccidentsGroupViewModel
    {
        public int Total { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public int Percentage { get; set; }
    }
}