﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AccidentsWeb.Models
{
    public class Accident
    {
        [Key]
        public int Id { get; set; }    
        [Required]
        public string Reference { get; set; }
        public string GridEasting { get; set; }
        public string GridNorthing { get; set; }
        public int VehicleCount { get; set; }
        public string VehicleType { get; set; }
        public string Express { get; set; }
        public DateTime AccidentDate { get; set; }
        public DateTime AccidentTime { get; set; }
        public string RoadClass { get; set; }
        public string RoadSurface { get; set; }
        public string LightingConditions { get; set; }
        public string WeatherConditions { get; set; }
        public string CasualtyClass { get; set; }
        public string CasualtySeverity { get; set; }
        public string CasualtySex { get; set; }
        public DateTime DateOfBirth { get; set; }
        
    }
}