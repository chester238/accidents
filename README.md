**Chester Thomas - Accidents Update**

This is an update of the original assessment.

*Updates include a fully functional Unit Tests project*

---

## Running the application

The solution is an MVC Web Application.

1. The solution requires **Visual Studio 2017**.
2. The current connection string is for SQL Server Express 2016 and will need to be updated to an instance of SQL appropriately in the web.config file.
3. Once connection string is updated, run the web application from Visual Studio.
4. There is a datatabase initialization that is automatic when the application is run and this includes a seed method that will add entries to the database.
5. Should the database not be updated automatically, run the **Update-Database** comman in Visual Studio Package Manager Console against the Web Project.

---

## The solution is also published as an Azure Website app

https://accidents-test.azurewebsites.net/

